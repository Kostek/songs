object Params {
    const val minSdkVersion = 19
    const val targetSdkVersion = 28
    const val compileSdkVersion = 29
    const val buildToolsVersion = "29.0.2"

    const val appVersion = 2
}