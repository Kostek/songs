package dependencies

object FunctionalTests {

    object Support {
        const val rules = "androidx.test:rules:1.2.0"
        const val runner = "androidx.test:runner:1.2.0"
        const val junit = "androidx.test.ext:junit:1.1.1"
    }

    object Espresso {
        const val core = "androidx.test.espresso:espresso-core:3.1.1"
    }
}
