package dependencies

object UnitTests {
    const val junit = "junit:junit:4.12"
    const val mockito = "org.mockito:mockito-core:3.1.0"
    const val mockWebServer = "com.squareup.okhttp3:mockwebserver:4.2.2"
    const val robolectric = "org.robolectric:robolectric:4.3.1"
    const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
    const val fragmentTesting = "androidx.fragment:fragment-testing:1.1.0-rc01"
    const val core = "androidx.test:core:1.2.0"

    object AssertJ {
        const val android = "com.squareup.assertj:assertj-android:1.2.0"
    }
}
