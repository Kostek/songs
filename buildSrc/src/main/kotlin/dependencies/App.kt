package dependencies

object App {

    object Support {
        const val appcompat = "androidx.appcompat:appcompat:1.0.2"
        const val constraint = "androidx.constraintlayout:constraintlayout:1.1.3"
        const val coreKtx = "androidx.core:core-ktx:1.0.2"
        const val recycler = "androidx.recyclerview:recyclerview:1.0.0"
        const val material = "com.google.android.material:material:1.0.0"
    }

    object Lifecycle {
        const val extensions = "androidx.lifecycle:lifecycle-extensions:2.1.0"
        const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.0.0"
        const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:2.1.0"
    }

    object Network {
        const val gson = "com.google.code.gson:gson:2.8.6"
        const val retrofit = "com.squareup.retrofit2:retrofit:2.6.2"
        const val rxjava = "io.reactivex.rxjava2:rxjava:2.2.13"
        const val rxandroid = "io.reactivex.rxjava2:rxandroid:2.1.1"
        const val rxRetrofitAdapter = "com.jakewharton.retrofit:retrofit2-rxjava2-adapter:1.0.0"
        const val gsonConverter = "com.squareup.retrofit2:converter-gson:2.6.2"
    }

    const val databinding = "com.android.databinding:compiler:2.3.1"
    const val picasso = "com.squareup.picasso:picasso:2.71828"
}
