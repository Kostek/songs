plugins {
    `kotlin-dsl`
}

repositories {
    maven {
        setUrl("https://plugins.gradle.org/m2/")
    }
}

dependencies {
    implementation("com.sun.codemodel:codemodel:2.6")
    implementation("commons-io:commons-io:2.6")
}
