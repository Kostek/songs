import dependencies.App

plugins {
    id(Plugins.library)
    kotlin("kapt")
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Params.compileSdkVersion)
    buildToolsVersion(Params.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Params.minSdkVersion)
        targetSdkVersion(Params.targetSdkVersion)
        vectorDrawables.useSupportLibrary = true
    }

    dataBinding.isEnabled = true
}

dependencies {
    kapt(App.databinding)
    implementation(App.Support.appcompat)
    implementation(App.Support.constraint)
    implementation(App.picasso)
}
