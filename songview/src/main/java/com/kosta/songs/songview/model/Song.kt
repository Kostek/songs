package com.kosta.songs.songview.model

import androidx.annotation.ColorRes

data class Song(
    val title: String,
    val artist: String,
    val previewImageUrl: String?,
    val releaseYear: String,
    @ColorRes val sourceIndicatorColor: Int
)