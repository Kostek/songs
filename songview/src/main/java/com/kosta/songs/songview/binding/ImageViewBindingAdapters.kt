package com.kosta.songs.songview.binding

import android.graphics.drawable.Drawable
import android.text.TextUtils
import androidx.annotation.ColorRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl", "error", "defaultPreview")
fun AppCompatImageView.loadImage(url: String?, error: Drawable, defaultPreview: Drawable) {
    if (!TextUtils.isEmpty(url)) {
        Picasso.get().load(url).error(error).into(this)
    } else {
        setImageDrawable(defaultPreview)
    }
}

@BindingAdapter("color")
fun AppCompatImageView.setColor(@ColorRes colorRes: Int) {
    setBackgroundResource(colorRes)
}