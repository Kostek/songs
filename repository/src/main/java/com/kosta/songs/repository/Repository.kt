package com.kosta.songs.repository

import androidx.lifecycle.MutableLiveData
import com.kosta.songs.logger.Logg
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class Repository<D> private constructor(builder: Builder<D>) {

    private val dataSources: Map<Int, DataSource<D>>
    private val mutableLiveData = MutableLiveData<List<D>>(emptyList())
    private val pageSize: Int
    private val disopsables = CompositeDisposable()

    init {
        this.dataSources = builder.dataSources.toMap()
        this.pageSize = builder.pageSize
    }

    class Builder<D> {
        val dataSources: MutableMap<Int, DataSource<D>> = mutableMapOf()
        var pageSize: Int = 15

        fun withDataSource(source: DataSource<D>) = apply {
            if (this.dataSources.contains(source.id())) {
                throw IllegalStateException("DataSource with id ${source.id()} was already added")
            }

            this.dataSources[source.id()] = source
        }

        fun withPageSize(pageSize: Int) = apply { this.pageSize = pageSize }

        fun build() = Repository(this)
    }

    fun getData(): MutableLiveData<List<D>> {
        return mutableLiveData
    }

    fun loadInitialPage(dataSourceIds: List<Int> = dataSources.keys.toList()) {
        load(dataSourceIds) { it.loadFirstPage(pageSize) }
    }

    fun loadNextPage(dataSourceIds: List<Int> = dataSources.keys.toList()) {
        load(dataSourceIds) { it.loadNextPage() }
    }

    fun loadPrevPage(dataSourceIds: List<Int> = dataSources.keys.toList()) {
        load(dataSourceIds) { it.loadPrevPage() }
    }

    private fun load(dataSourceIds: List<Int>, method: (DataSource<D>) -> Observable<List<D>>) {
        disopsables.add(Observable.merge(dataSources
            .filter { dataSourceIds.contains(it.key) }
            .map {
                method(it.value).onErrorReturn { throwable ->
                    Logg.e(
                        "Error while loading data from ${it.value.javaClass.name}",
                        throwable
                    )
                    emptyList()
                }
            })
            .reduce { left, right -> left.toMutableList().apply { addAll(right) } }
            .subscribe {
                Logg.i("Loaded ${it.size} items")
                val newSet = (mutableLiveData.value ?: emptyList()).toMutableList()
                newSet.addAll(it)
                mutableLiveData.postValue(newSet)
            }
        )
    }

    fun terminateAll() {
        disopsables.clear()
    }

    fun clear() {
        terminateAll()
        mutableLiveData.value = emptyList()
    }
}