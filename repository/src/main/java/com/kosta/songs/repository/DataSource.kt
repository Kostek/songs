package com.kosta.songs.repository

import io.reactivex.Observable

interface DataSource<D> {
    fun id(): Int
    fun loadFirstPage(pageSize: Int): Observable<List<D>>
    fun loadNextPage(): Observable<List<D>>
    fun loadPrevPage(): Observable<List<D>>
}