package com.kosta.songs.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.kosta.songs.repository.Repository.Builder
import com.kosta.songs.testutils.ImmediateRxRule
import io.reactivex.Observable
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class RepositoryTest {

    data class Item(val name: String)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxRule = ImmediateRxRule()

    @Mock
    lateinit var firstDataSource: DataSource<Item>

    @Mock
    lateinit var secondDataSource: DataSource<Item>

    private lateinit var repository: Repository<Item>
    private val pageSize = 1

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        given(firstDataSource.id()).willReturn(0)
        given(firstDataSource.loadFirstPage(pageSize)).willReturn(Observable.just(emptyList()))
        given(firstDataSource.loadNextPage()).willReturn(Observable.just(emptyList()))
        given(firstDataSource.loadPrevPage()).willReturn(Observable.just(emptyList()))

        given(secondDataSource.id()).willReturn(1)
        given(secondDataSource.loadFirstPage(pageSize)).willReturn(Observable.just(emptyList()))
        given(secondDataSource.loadNextPage()).willReturn(Observable.just(emptyList()))
        given(secondDataSource.loadPrevPage()).willReturn(Observable.just(emptyList()))

        repository = Builder<Item>()
            .withDataSource(firstDataSource)
            .withDataSource(secondDataSource)
            .withPageSize(pageSize)
            .build()
    }

    @Test
    fun shouldTriggerLoadingInitialPageOnAllDataSources() {
        // when
        repository.loadInitialPage()

        // then
        verify(firstDataSource).loadFirstPage(pageSize)
        verify(secondDataSource).loadFirstPage(pageSize)
    }

    @Test
    fun shouldTriggerOnlySelectedDataSource() {
        // when
        repository.loadInitialPage(listOf(secondDataSource.id()))

        // then
        verify(firstDataSource, times(0)).loadFirstPage(pageSize)
        verify(secondDataSource).loadFirstPage(pageSize)
    }

    @Test
    fun shouldTriggerLoadingNextPageOnAllDataSources() {
        // when
        repository.loadNextPage()

        // then
        verify(firstDataSource).loadNextPage()
        verify(secondDataSource).loadNextPage()
    }

    @Test
    fun shouldTriggerLoadingPrevPageOnAllDataSources() {
        // when
        repository.loadPrevPage()

        // then
        verify(firstDataSource).loadPrevPage()
        verify(secondDataSource).loadPrevPage()
    }

    @Test
    fun shouldContainSongsFromAllDataSourcesWhenInitialLoadCalled() {
        // given
        val item1name = "Title 1"
        val item1 = Item(item1name)
        val item2name = "Title 2"
        val item2 = Item(item2name)

        given(firstDataSource.loadFirstPage(pageSize)).willReturn(
            Observable.just(
                Collections.singletonList(
                    item1
                )
            )
        )
        given(secondDataSource.loadFirstPage(pageSize)).willReturn(
            Observable.just(
                Collections.singletonList(
                    item2
                )
            )
        )
        val mutableData = repository.getData()

        // when
        repository.loadInitialPage()

        // then
        assertThat(mutableData.value).hasSize(2)
    }

    @Test
    fun shouldContainInitialAndNextPageitemsWhenLoadingNextPage() {
        // given
        val item1 = Item("Title 1")
        val item2 = Item("Title 2")
        val item3 = Item("Title 3")
        val item4 = Item("Title 4")

        given(firstDataSource.loadFirstPage(pageSize)).willReturn(
            Observable.just(
                Collections.singletonList(
                    item1
                )
            )
        )
        given(firstDataSource.loadNextPage()).willReturn(
            Observable.just(
                Collections.singletonList(
                    item2
                )
            )
        )
        given(secondDataSource.loadFirstPage(pageSize)).willReturn(
            Observable.just(
                Collections.singletonList(
                    item3
                )
            )
        )
        given(secondDataSource.loadNextPage()).willReturn(
            Observable.just(
                Collections.singletonList(
                    item4
                )
            )
        )
        val mutableData = repository.getData()
        repository.loadInitialPage()

        // when
        repository.loadNextPage()

        // then
        assertThat(mutableData.value).hasSize(4)
    }
}