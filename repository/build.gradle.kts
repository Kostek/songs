import dependencies.App
import dependencies.UnitTests

plugins {
    id(Plugins.library)
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Params.compileSdkVersion)
    buildToolsVersion(Params.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Params.minSdkVersion)
        targetSdkVersion(Params.targetSdkVersion)
    }
}

dependencies {
    implementation(project(":logger"))
    implementation(project(":testutils"))
    implementation(App.Lifecycle.livedata)
    implementation(App.Network.rxjava)
    implementation(App.Network.rxandroid)

    testImplementation(UnitTests.junit)
    testImplementation(UnitTests.mockito)
    testImplementation(UnitTests.AssertJ.android)
    testImplementation(UnitTests.coreTesting)
}
