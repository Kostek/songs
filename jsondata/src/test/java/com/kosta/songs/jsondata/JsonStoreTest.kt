package com.kosta.songs.jsondata

import com.kosta.songs.testutils.ImmediateRxRule
import com.kosta.songs.testutils.TestUtils
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class JsonStoreTest {

    @get:Rule
    val rule = ImmediateRxRule()

    private lateinit var jsonStore: JsonStore

    @Before
    fun setUp() {
        val inputStream = TestUtils.getTestFile("ten_songs.json").inputStream()
        jsonStore = JsonStore(object : JsonStreamProvider {
            override fun open() = inputStream

            override fun close() = inputStream.close()
        })
    }

    @Test
    fun shouldReadFirstFiveSongs() {
        // given
        val expectedFirstArtistName = "Artist 1"
        val expectedLastArtistName = "Artist 5"

        // when
        val actual = jsonStore.readSongs(5, 0).blockingSingle()

        // then
        assertThat(actual).hasSize(5)
        assertThat(actual[0].artistName).isEqualTo(expectedFirstArtistName)
        assertThat(actual[4].artistName).isEqualTo(expectedLastArtistName)
    }

    @Test
    fun shouldReadFourthSongOnly() {
        // given
        val expectedArtistName = "Artist 4"

        // when
        val actual = jsonStore.readSongs(1, 3).blockingSingle()

        // then
        assertThat(actual).hasSize(1)
        assertThat(actual[0].artistName).isEqualTo(expectedArtistName)
    }
}
