package com.kosta.songs.jsondata

import com.google.gson.Gson
import com.google.gson.stream.JsonReader
import com.kosta.songs.jsondata.model.Song
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import io.reactivex.schedulers.Schedulers
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets

class JsonStore(private val jsonStreamPrivider: JsonStreamProvider) {

    fun readSongs(pageSize: Int = 25, pageNumber: Int = 0): Observable<List<Song>> {
        var localPageSize = pageSize
        if (pageSize > 200) localPageSize = 200
        else if (pageSize < 0) localPageSize = 0

        val pageStartPosition = pageNumber * localPageSize
        val pageEndPosition = pageStartPosition + localPageSize

        return readPage(pageStartPosition, pageEndPosition)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
    }

    private fun readPage(pageStartPosition: Int, pageEndPosition: Int) =
        Observable.create(ObservableOnSubscribe<List<Song>> { emitter ->
            var reader: JsonReader? = null
            val songs = ArrayList<Song>(pageEndPosition - pageStartPosition)
            val gson = Gson()

            try {
                reader = JsonReader(
                    InputStreamReader(
                        jsonStreamPrivider.open(),
                        StandardCharsets.UTF_8
                    )
                )
                reader.beginArray()

                var position = 0

                while (reader.hasNext()) {
                    if (position in pageStartPosition until pageEndPosition) {
                        val song = gson.fromJson(reader, Song::class.java) as Song
                        songs.add(song)
                    } else {
                        reader.skipValue()
                    }
                    position++
                }

                reader.endArray()
            } finally {
                reader?.close()
                jsonStreamPrivider.close()
            }

            emitter.onNext(songs)
            emitter.onComplete()
        })
}