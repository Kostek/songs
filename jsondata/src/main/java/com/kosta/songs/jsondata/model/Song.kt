package com.kosta.songs.jsondata.model

import com.google.gson.annotations.SerializedName

data class Song(
    @SerializedName("Song Clean") val trackName: String,
    @SerializedName("ARTIST CLEAN") val artistName: String,
    @SerializedName("Release Year") val releaseYear: String,
    @SerializedName("COMBINED") val combinedName: String,
    @SerializedName("First?") val first: Int,
    @SerializedName("Year?") val year: Int,
    @SerializedName("PlayCount") val playCount: Int,
    @SerializedName("F*G") val fg: Int
)