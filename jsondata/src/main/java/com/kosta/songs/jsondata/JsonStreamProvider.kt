package com.kosta.songs.jsondata

import java.io.InputStream

interface JsonStreamProvider {
    fun open(): InputStream
    fun close()
}