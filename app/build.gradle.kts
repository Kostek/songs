import com.android.build.gradle.internal.api.ApkVariantOutputImpl
import dependencies.App
import dependencies.FunctionalTests
import dependencies.UnitTests

plugins {
    id(Plugins.application)
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Params.compileSdkVersion)
    buildToolsVersion(Params.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Params.minSdkVersion)
        targetSdkVersion(Params.targetSdkVersion)
        applicationId = rootProject.property("android.build.applicationId") as String
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    dataBinding.isEnabled = true

    signingConfigs {
        getByName("debug") {
            storeFile = file("${rootDir.path}/dev-debug.keystore")
            storePassword = "android"
            keyAlias = "androiddebugkey"
            keyPassword = "android"
        }

        create("release") {
            storeFile = file("${rootDir.path}/dev-release.keystore")
            storePassword = "android"
            keyAlias = "androidreleasekey"
            keyPassword = "android"
        }
    }

    buildTypes {
        getByName("debug") {
            signingConfig = signingConfigs.getByName("debug")
            val debugSuffix = rootProject.property("android.build.debugSuffix")
            applicationIdSuffix = ".$debugSuffix"
            versionNameSuffix = "-$debugSuffix"
        }

        getByName("release") {
            signingConfig = signingConfigs.getByName("release")
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    testOptions.unitTests.isIncludeAndroidResources = true

    android.applicationVariants.all {
        outputs.forEach { output ->
            if (output is ApkVariantOutputImpl) {
                output.versionCodeOverride = Params.appVersion
                val buildType = this@all.buildType.name
                output.versionNameOverride =
                    "${Params.appVersion}${if (buildType == "debug") "-$buildType" else ""}"
            }
        }
    }
}

dependencies {
    implementation(App.Support.appcompat)
    implementation(App.Support.constraint)
    implementation(App.Support.material)
    implementation(App.Support.recycler)
    implementation(App.Support.coreKtx)
    implementation(App.Lifecycle.extensions)
    implementation(App.Lifecycle.viewmodel)
    implementation(App.Network.rxjava)
    implementation(App.Lifecycle.livedata)

    implementation(project(":songview"))
    implementation(project(":repository"))
    implementation(project(":jsondata"))
    implementation(project(":itunesdata"))

    testImplementation(UnitTests.junit)
    implementation(UnitTests.fragmentTesting) {
        exclude(group = "androidx.test", module = "monitor")
        exclude(group = "androidx.test", module = "core")
    }
    testImplementation(UnitTests.mockito)
    testImplementation(UnitTests.robolectric)
    testImplementation(UnitTests.AssertJ.android)
    testImplementation(UnitTests.core)

    androidTestImplementation(FunctionalTests.Support.runner)
    androidTestImplementation(FunctionalTests.Support.junit)
    androidTestImplementation(FunctionalTests.Support.rules)
    androidTestImplementation(FunctionalTests.Espresso.core)
}
