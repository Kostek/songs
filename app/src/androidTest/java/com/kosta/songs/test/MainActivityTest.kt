package com.kosta.songs.test

import androidx.test.espresso.IdlingRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.kosta.songs.MainActivity
import com.kosta.songs.model.Source
import com.kosta.songs.usecase.MainActivityUseCases.Companion.clickSource
import com.kosta.songs.usecase.MainActivityUseCases.Companion.openContextMenu
import com.kosta.songs.usecase.MainActivityUseCases.Companion.validateListSize
import com.kosta.songs.util.RxIdlingResource
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        val rxIdlingResource = RxIdlingResource()
        IdlingRegistry.getInstance().register(rxIdlingResource)
        RxJavaPlugins.setScheduleHandler(rxIdlingResource)
        activityRule.finishActivity()
        activityRule.launchActivity(null)
    }

    @Test
    fun unselectItunesSource() {
        validateListSize(40)

        openContextMenu()

        clickSource(Source.ITUNES)

        validateListSize(20)
    }
}
