package com.kosta.songs.matcher

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher

class RecyclerViewMatchers {
    companion object {
        fun hasNumberOfItems(size: Int): Matcher<View> =
            object : BaseMatcher<View>() {
                override fun describeMismatch(item: Any?, description: Description) {
                    if (item == null) {
                        description.appendText("was null")
                    } else if (item !is RecyclerView) {
                        description.appendText("was a ")
                            .appendText(item.javaClass.name)
                            .appendText(" (")
                            .appendValue(item)
                            .appendText(")")
                    } else {
                        description.appendText("Current number of list items: " + item.adapter?.itemCount)
                    }
                }

                override fun describeTo(description: Description) {
                    description.appendText("Number of list items in the list view: $size")
                }

                override fun matches(item: Any) =
                    item is RecyclerView && item.adapter?.itemCount == size
            }
    }
}