package com.kosta.songs.selector

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers.*
import com.kosta.songs.R
import com.kosta.songs.model.Source
import org.hamcrest.CoreMatchers.allOf

class MainActivitySelector {
    companion object {

        fun onToolbarMenuItem(source: Source): ViewInteraction = onView(
            withText(
                when (source) {
                    Source.ITUNES -> "iTunes songs"
                    else -> "Json songs"
                }
            )
        ).check(
            ViewAssertions.matches(
                isCompletelyDisplayed()
            )
        )

        fun onSongsList(): ViewInteraction =
            onView(allOf(withId(R.id.songsList), isDescendantOfA(withId(R.id.list)))).check(
                ViewAssertions.matches(
                    isCompletelyDisplayed()
                )
            )
    }
}