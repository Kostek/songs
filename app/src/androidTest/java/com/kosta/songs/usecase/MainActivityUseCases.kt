package com.kosta.songs.usecase

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import com.kosta.songs.matcher.RecyclerViewMatchers.Companion.hasNumberOfItems
import com.kosta.songs.model.Source
import com.kosta.songs.selector.MainActivitySelector.Companion.onSongsList
import com.kosta.songs.selector.MainActivitySelector.Companion.onToolbarMenuItem

class MainActivityUseCases {
    companion object {
        fun openContextMenu() {
            Espresso.openContextualActionModeOverflowMenu()
        }

        fun clickSource(source: Source) {
            onToolbarMenuItem(source).perform(click())
        }

        fun validateListSize(size: Int) {
            onSongsList().check(matches(hasNumberOfItems(size)))
        }
    }
}