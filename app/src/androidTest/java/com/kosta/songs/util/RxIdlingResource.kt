package com.kosta.songs.util

/*
 * The MIT License
 *
 * Copyright (c) 2016 Andreas Ahlenstorf
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import androidx.test.espresso.IdlingResource
import io.reactivex.functions.Function

import java.util.concurrent.locks.ReentrantReadWriteLock

class RxIdlingResource : IdlingResource, Function<Runnable, Runnable> {

    // Guarded by IDLING_STATE_LOCK
    private var taskCount = 0

    // Guarded by IDLING_STATE_LOCK
    private var transitionCallback: IdlingResource.ResourceCallback? = null

    override fun getName(): String {
        return RESOURCE_NAME
    }

    override fun isIdleNow(): Boolean {
        IDLING_STATE_LOCK.readLock().lock()
        val result = taskCount == 0
        IDLING_STATE_LOCK.readLock().unlock()

        return result
    }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback) {
        IDLING_STATE_LOCK.writeLock().lock()
        this.transitionCallback = callback
        IDLING_STATE_LOCK.writeLock().unlock()
    }

    override fun apply(runnable: Runnable): Runnable {
        return Runnable {
            IDLING_STATE_LOCK.writeLock().lock()
            taskCount++
            IDLING_STATE_LOCK.writeLock().unlock()

            try {
                runnable.run()
            } finally {
                IDLING_STATE_LOCK.writeLock().lock()

                try {
                    taskCount--

                    if (taskCount == 0 && transitionCallback != null) {
                        transitionCallback!!.onTransitionToIdle()
                    }
                } finally {
                    IDLING_STATE_LOCK.writeLock().unlock()
                }
            }
        }
    }

    companion object {

        private val RESOURCE_NAME = RxIdlingResource::class.java.simpleName

        private val IDLING_STATE_LOCK = ReentrantReadWriteLock()
    }
}