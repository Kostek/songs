package com.kosta.songs

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.iterator
import com.kosta.songs.model.Source.Companion.fromMenuItemId
import com.kosta.songs.viewmodel.SongsListFragmenViewModelProvider
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        val viewModel = SongsListFragmenViewModelProvider.from(this)

        toolbar.setTitle(R.string.app_name)
        toolbar.inflateMenu(R.menu.source_selection)
        toolbar.menu.iterator().forEach {
            it.isChecked = viewModel.isSourceEnabled(fromMenuItemId(it.itemId))
        }

        toolbar.setOnMenuItemClickListener {
            it.isChecked = !it.isChecked
            viewModel.setSourceEnabled(fromMenuItemId(it.itemId), it.isChecked)

            false
        }
    }
}
