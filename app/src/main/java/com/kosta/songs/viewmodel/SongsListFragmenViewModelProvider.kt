package com.kosta.songs.viewmodel

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.kosta.songs.model.SongsRepositoryImpl

class SongsListFragmenViewModelProvider {
    companion object {
        fun from(activity: FragmentActivity) =
            ViewModelProviders.of(activity, BaseViewModelFactory {
                SongsListFragmentViewModel(SongsRepositoryImpl(activity))
            })[SongsListFragmentViewModel::class.java]
    }
}