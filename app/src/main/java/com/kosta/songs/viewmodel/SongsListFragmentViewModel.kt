package com.kosta.songs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.kosta.songs.model.Song
import com.kosta.songs.model.SongsRepository
import com.kosta.songs.model.Source

class SongsListFragmentViewModel(private val songsRepository: SongsRepository) : ViewModel() {

    private val sources = mutableListOf<Source>().apply { addAll(Source.values()) }

    fun getSongsLiveData(): LiveData<List<Song>> = songsRepository.getSongs()

    fun refresh() = songsRepository.apply {
        terminateAll()
        clear()
        loadInitialPage(this@SongsListFragmentViewModel.sources)
    }

    fun setSourceEnabled(source: Source, enabled: Boolean) {
        this.sources.apply {
            if (!enabled) remove(source)
            else if (!contains(source)) add(source)
        }

        refresh()
    }

    fun isSourceEnabled(source: Source) = sources.contains(source)

    override fun onCleared() {
        super.onCleared()
        songsRepository.terminateAll()
    }
}