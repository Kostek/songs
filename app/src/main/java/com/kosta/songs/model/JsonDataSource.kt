package com.kosta.songs.model

import android.content.Context
import com.kosta.songs.jsondata.JsonStore
import com.kosta.songs.jsondata.JsonStreamProvider
import com.kosta.songs.repository.DataSource
import io.reactivex.Observable
import java.io.InputStream
import com.kosta.songs.jsondata.model.Song as JsonSong
import com.kosta.songs.model.Song as RepoSong

class JsonDataSource(context: Context) : DataSource<RepoSong> {

    private val jsonStreamProvider = object : JsonStreamProvider {
        var stream: InputStream? = null

        override fun open(): InputStream {
            stream = context.assets.open("songs-list.json")
            return stream!!
        }

        override fun close() {
            stream?.close()
        }
    }

    private val jsonStore = JsonStore(jsonStreamProvider)
    private var currentPage = 0
    private var pageSize = 1

    override fun id(): Int {
        return Source.JSON.ordinal
    }

    override fun loadFirstPage(pageSize: Int): Observable<List<RepoSong>> {
        this.pageSize = pageSize
        return load(pageSize, currentPage)
    }

    override fun loadNextPage(): Observable<List<RepoSong>> =
        load(pageSize, currentPage++)

    override fun loadPrevPage(): Observable<List<RepoSong>> =
        if (currentPage > 0) {
            load(pageSize, currentPage--)
        } else {
            Observable.just(emptyList())
        }

    private fun load(pageSize: Int, pageNumber: Int) =
        jsonStore.readSongs(pageSize, pageNumber).map { it.map { song -> convert(song) } }

    private fun convert(song: JsonSong): RepoSong =
        RepoSong(song.trackName, song.artistName, null, song.releaseYear, id())
}