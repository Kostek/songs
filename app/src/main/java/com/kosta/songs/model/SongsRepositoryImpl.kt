package com.kosta.songs.model

import android.content.Context
import com.kosta.songs.repository.Repository

class SongsRepositoryImpl(context: Context) : SongsRepository {

    private val repository = Repository.Builder<Song>()
        .withPageSize(20)
        .withDataSource(ITunesDataSource())
        .withDataSource(JsonDataSource(context))
        .build()

    init {
        loadInitialPage()
    }

    override fun getSongs() = repository.getData()

    private fun loadInitialPage() = loadInitialPage(Source.values().toList())

    override fun loadInitialPage(dataSourceIds: List<Source>) =
        repository.loadInitialPage(dataSourceIds.map { it.ordinal })

    override fun terminateAll() = repository.terminateAll()

    override fun clear() = repository.clear()
}