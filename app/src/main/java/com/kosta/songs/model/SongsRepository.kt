package com.kosta.songs.model

import androidx.lifecycle.MutableLiveData

interface SongsRepository {
    fun getSongs(): MutableLiveData<List<Song>>

    fun loadInitialPage(dataSourceIds: List<Source>)

    fun terminateAll()

    fun clear()
}