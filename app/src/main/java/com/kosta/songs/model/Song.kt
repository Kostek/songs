package com.kosta.songs.model

data class Song(
    val title: String,
    val artist: String,
    val previewImageUrl: String?,
    val releaseYear: String,
    val sourceId: Int
)