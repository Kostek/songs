package com.kosta.songs.model

import com.kosta.songs.itunesdata.ITunesStore
import com.kosta.songs.repository.DataSource
import io.reactivex.Observable
import java.util.*
import com.kosta.songs.model.Song as RepoSong

class ITunesDataSource : DataSource<RepoSong> {
    override fun id(): Int {
        return Source.ITUNES.ordinal
    }

    private val iTunesStore = ITunesStore()

    override fun loadFirstPage(pageSize: Int): Observable<List<RepoSong>> {
        val calendar = Calendar.getInstance()

        return iTunesStore.fetchSongs("G-Eazy", pageSize)
            .map {
                it.map { song ->
                    calendar.time = song.releaseDate
                    RepoSong(
                        song.trackName,
                        song.artistName,
                        song.artworkUrl60,
                        calendar.get(Calendar.YEAR).toString(),
                        id()
                    )
                }
            }
            .toObservable()
    }

    override fun loadNextPage(): Observable<List<RepoSong>> =
        Observable.just(emptyList())

    override fun loadPrevPage(): Observable<List<RepoSong>> =
        Observable.just(emptyList())
}