package com.kosta.songs.model

import androidx.annotation.ColorRes
import androidx.annotation.IdRes
import com.kosta.songs.R

enum class Source(@IdRes val menuItemId: Int, @ColorRes val indicatorColor: Int) {
    ITUNES(R.id.itunes, R.color.itunes_indicator), JSON(R.id.json, R.color.json_indicator);

    companion object {
        fun from(ordinal: Int) = values().first { it.ordinal == ordinal }
        fun fromMenuItemId(@IdRes menuItemId: Int) = when (menuItemId) {
            ITUNES.menuItemId -> ITUNES
            else -> JSON
        }
    }
}