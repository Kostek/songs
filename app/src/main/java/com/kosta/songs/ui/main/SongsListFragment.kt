package com.kosta.songs.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kosta.songs.R
import com.kosta.songs.adapter.SongsAdapter
import com.kosta.songs.databinding.SongsFragmentBinding
import com.kosta.songs.model.Source
import com.kosta.songs.songview.model.Song

import com.kosta.songs.viewmodel.SongsListFragmenViewModelProvider
import com.kosta.songs.viewmodel.SongsListFragmentViewModel

class SongsListFragment : Fragment() {

    private lateinit var model: SongsListFragmentViewModel
    private var binding: SongsFragmentBinding? = null
    private var songsAdapter: SongsAdapter = SongsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = activity?.run {
            SongsListFragmenViewModelProvider.from(this)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.songs_fragment,
            container,
            false
        ) as SongsFragmentBinding

        return binding!!.swipeLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding!!.apply {
            songsList.layoutManager =
                LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            songsList.adapter = songsAdapter
            swipeLayout.setColorSchemeResources(R.color.colorPrimaryDark)
            swipeLayout.setOnRefreshListener {
                model.refresh()
            }
        }

        model.apply {
            getSongsLiveData().observe(this@SongsListFragment, Observer {
                binding?.swipeLayout?.isRefreshing = false
                songsAdapter.setSongs(it.map { song ->
                    Song(
                        song.title,
                        song.artist,
                        song.previewImageUrl,
                        song.releaseYear,
                        Source.from(song.sourceId).indicatorColor
                    )
                })
            })
        }
    }
}
