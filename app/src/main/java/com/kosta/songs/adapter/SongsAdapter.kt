package com.kosta.songs.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.kosta.songs.R
import com.kosta.songs.songview.databinding.ItemSongBinding
import com.kosta.songs.songview.model.Song

class SongsAdapter : RecyclerView.Adapter<SongsAdapter.SongViewHolder>() {

    private var songs = mutableListOf<Song>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SongViewHolder {
        val itemSongBinding: ItemSongBinding = DataBindingUtil.inflate(
            LayoutInflater.from(viewGroup.context),
            R.layout.item_song, viewGroup, false
        )
        return SongViewHolder(itemSongBinding)
    }

    override fun onBindViewHolder(songViewHolder: SongViewHolder, i: Int) {
        songViewHolder.songBinding.song = songs[i]
    }

    override fun getItemCount() = songs.size

    fun setSongs(songs: List<Song>) {
        this.songs.clear()
        this.songs.addAll(songs)
        notifyDataSetChanged()
    }

    inner class SongViewHolder(val songBinding: ItemSongBinding) :
        RecyclerView.ViewHolder(songBinding.root)
}
