package com.kosta.songs.model

import android.content.Context
import androidx.lifecycle.MutableLiveData

class SongsRepositoryImpl(context: Context) : SongsRepository {

    companion object {
        val mutableData = MutableLiveData<List<Song>>(emptyList())

        fun setData(data: List<Song>) {
            mutableData.value = data
        }
    }

    override fun getSongs() = mutableData

    override fun loadInitialPage(dataSourceIds: List<Source>) {}

    override fun terminateAll() {}

    override fun clear() {}
}