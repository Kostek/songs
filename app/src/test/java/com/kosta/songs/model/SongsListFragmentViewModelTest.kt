package com.kosta.songs.model

import com.kosta.songs.viewmodel.SongsListFragmentViewModel
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class SongsListFragmentViewModelTest {

    @Mock
    private lateinit var repository: SongsRepository

    private lateinit var testObj: SongsListFragmentViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        testObj = SongsListFragmentViewModel(repository)
    }

    @Test
    fun shouldReturnSourceEnabledIfSourceInList() {
        // given
        val source = Source.ITUNES
        testObj.setSourceEnabled(source, true)

        // when
        val actual = testObj.isSourceEnabled(source)

        // then
        assertThat(actual).isTrue()
    }

    @Test
    fun shouldReturnOneSourceEnabledWhenSourceRemoved() {
        // given
        val source1 = Source.ITUNES
        val source2 = Source.JSON
        testObj.setSourceEnabled(source1, true)
        testObj.setSourceEnabled(source2, true)

        // when
        testObj.setSourceEnabled(source1, false)

        // then
        assertThat(testObj.isSourceEnabled(source1)).isFalse()
        assertThat(testObj.isSourceEnabled(source2)).isTrue()
    }
}