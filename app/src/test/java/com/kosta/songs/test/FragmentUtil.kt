package com.kosta.songs.test

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentFactory
import androidx.fragment.app.testing.FragmentScenario

class FragmentUtil {
    companion object {
        fun start(fragment: Fragment) {
            FragmentScenario.launchInContainer(
                fragment.javaClass,
                fragment.arguments,
                object : FragmentFactory() {
                    override fun instantiate(
                        classLoader: ClassLoader,
                        className: String
                    ): Fragment {
                        return if (className == fragment.javaClass.name) {
                            fragment
                        } else super.instantiate(classLoader, className)
                    }
                })
        }
    }
}