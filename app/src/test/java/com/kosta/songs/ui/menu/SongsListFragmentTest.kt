package com.kosta.songs.ui.menu

import com.kosta.songs.model.Song
import com.kosta.songs.model.SongsRepositoryImpl
import com.kosta.songs.model.Source
import com.kosta.songs.test.FragmentUtil
import com.kosta.songs.ui.main.SongsListFragment
import kotlinx.android.synthetic.main.songs_fragment.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class SongsListFragmentTest {

    @Test
    fun shouldShowTwoSongsInFragment() {
        // given
        val song1 = Song("Song 1", "Artist 1", null, "2001", Source.ITUNES.ordinal)
        val song2 = Song("Song 2", "Artist 2", null, "2002", Source.JSON.ordinal)
        SongsRepositoryImpl.setData(listOf(song1, song2))

        val fragment = SongsListFragment()

        // when
        FragmentUtil.start(fragment)

        // then
        assertThat(fragment.songsList).isNotNull()
        assertThat(fragment.songsList.adapter).isNotNull()
        assertThat(fragment.songsList.adapter?.itemCount).isEqualTo(2)
    }
}
