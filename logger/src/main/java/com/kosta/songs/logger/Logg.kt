package com.kosta.songs.logger

import android.util.Log

class Logg {
    companion object {
        private const val TAG = "Songs"

        fun i(msg: String) = Log.i(TAG, msg)
        fun e(msg: String, throwable: Throwable) = Log.e(TAG, msg, throwable)
    }
}