package com.kosta.songs.testutils

import java.io.File
import java.nio.charset.StandardCharsets

class TestUtils {
    companion object {
        private val TEST_DATA_PATH =
            "src${File.separator}test${File.separator}data${File.separator}"

        fun getTestFile(fileName: String) = File(pathToTestFile(fileName))

        fun readJsonFile(fileName: String) = readFile(pathToTestFile(fileName))

        private fun pathToTestFile(fileName: String) = "$TEST_DATA_PATH$fileName"

        private fun readFile(filePath: String) = File(filePath).readText(StandardCharsets.UTF_8)
    }
}
