import dependencies.App
import dependencies.UnitTests

plugins {
    id(Plugins.library)
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Params.compileSdkVersion)
    buildToolsVersion(Params.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Params.minSdkVersion)
        targetSdkVersion(Params.targetSdkVersion)
    }
}

dependencies {
    implementation(App.Network.rxjava)
    implementation(App.Network.rxandroid)
    implementation(UnitTests.junit)
}
