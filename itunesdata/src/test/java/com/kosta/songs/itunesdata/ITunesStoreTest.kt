package com.kosta.songs.itunesdata

import com.kosta.songs.itunesdata.model.WrappedType
import com.kosta.songs.testutils.ImmediateRxRule
import com.kosta.songs.testutils.TestUtils
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class ITunesStoreTest {

    @get:Rule
    val rule = ImmediateRxRule()

    private val mockServer: MockWebServer = MockWebServer()

    private lateinit var itunesStore: ITunesStore

    @Before
    @Throws(Exception::class)
    fun setUp() {
        mockServer.start()
        itunesStore = ITunesStore(mockServer.url("/").toString())
    }

    @After
    fun tearDown() {
        mockServer.shutdown()
    }

    @Test
    fun returnSongsOnSuccessfullCall() {
        // given
        val expectedSize = 4
        val fileName = "multiple_songs_response.json"
        mockServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(TestUtils.readJsonFile(fileName))
        )

        // when
        val actual = itunesStore.fetchSongs("anyterm").blockingSingle()

        // then
        assertThat(actual).hasSize(expectedSize)
    }

    @Test
    fun returnProperSongOnSuccessfullCall() {
        // given
        val expectedSize = 1
        val expectedWrapperType = WrappedType.TRACK
        val expectedArtistId = 909253L
        val expectedTrackId = 1440857786L
        val expectedArtistName = "Jack Johnson"
        val expectedCollectionName = "In Between Dreams (Bonus Track Version)"
        val expectedTrackName = "Better Together"

        val fileName = "single_song_response.json"
        mockServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(TestUtils.readJsonFile(fileName))
        )

        // when
        val actual = itunesStore.fetchSongs("anyterm").blockingSingle()

        // then
        assertThat(actual).hasSize(expectedSize)
        val actualSong = actual[0]
        assertThat(actualSong.wrapperType).isEqualTo(expectedWrapperType)
        assertThat(actualSong.artistId).isEqualTo(expectedArtistId)
        assertThat(actualSong.trackId).isEqualTo(expectedTrackId)
        assertThat(actualSong.artistName).isEqualTo(expectedArtistName)
        assertThat(actualSong.collectionName).isEqualTo(expectedCollectionName)
        assertThat(actualSong.trackName).isEqualTo(expectedTrackName)
    }

    @Test
    fun returnValidYearOnSuccessfullCall() {
        // given
        val expectedSize = 1
        val expectedYear = 2005

        val fileName = "single_song_response.json"
        mockServer.enqueue(
            MockResponse()
                .setResponseCode(200)
                .setBody(TestUtils.readJsonFile(fileName))
        )

        // when
        val actual = itunesStore.fetchSongs("anyterm").blockingSingle()

        // then
        assertThat(actual).hasSize(expectedSize)
        val actualSong = actual[0]
        val calendar = Calendar.getInstance()
        calendar.time = actualSong.releaseDate
        assertThat(calendar.get(Calendar.YEAR)).isEqualTo(expectedYear)
    }
}
