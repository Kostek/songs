package com.kosta.songs.itunesdata.model

import com.google.gson.annotations.SerializedName

enum class WrappedType {
    @SerializedName("track")
    TRACK,

    @SerializedName("collection")
    COLLECTION,

    @SerializedName("artistFor")
    ARTIST_FOR
}