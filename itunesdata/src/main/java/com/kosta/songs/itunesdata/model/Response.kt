package com.kosta.songs.itunesdata.model

internal data class Response(val resultsCount: Int, val results: List<Song>)