package com.kosta.songs.itunesdata.service

import com.kosta.songs.itunesdata.model.Response
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

internal interface ITunesService {

    companion object {
        const val BASE_URL = "https://itunes.apple.com/"
    }

    @GET("search?media=music&entity=song")
    fun searchSongs(
        @Query("term") term: String,
        @Query("limit") limit: Int = 25
    ): Flowable<Response>
}