package com.kosta.songs.itunesdata

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.kosta.songs.itunesdata.model.Song
import com.kosta.songs.itunesdata.service.ITunesService
import com.kosta.songs.itunesdata.util.GsonFactory.createGson
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ITunesStore(baseUrl: String = ITunesService.BASE_URL) {

    private val retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create(createGson()))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    fun fetchSongs(
        term: String,
        limit: Int = 25
    ): Flowable<List<Song>> {
        var localLimit = limit
        if (limit > 200) localLimit = 200
        else if (limit < 0) localLimit = 0

        return retrofit.create(ITunesService::class.java).searchSongs(term, localLimit)
            .subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .map { it.results }
    }
}