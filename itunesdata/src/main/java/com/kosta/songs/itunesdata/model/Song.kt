package com.kosta.songs.itunesdata.model

import java.util.*

data class Song(
    val wrapperType: WrappedType,
    val artistId: Long,
    val collectionId: Long,
    val trackId: Long,
    val artistName: String,
    val collectionName: String,
    val releaseDate: Date,
    val trackName: String,
    val artistViewUrl: String,
    val trackViewUrl: String,
    val artworkUrl60: String,
    val trackTimeMillis: Long,
    val primaryGenreName: String
)