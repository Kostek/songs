package com.kosta.songs.itunesdata.util

import com.google.gson.Gson
import com.google.gson.GsonBuilder

internal object GsonFactory {

    private const val DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'"

    fun createGson(): Gson {
        return GsonBuilder()
            .setDateFormat(DATE_PATTERN)
            .create()
    }
}
