import dependencies.App
import dependencies.UnitTests

plugins {
    id(Plugins.library)
    kotlin("android")
    kotlin("android.extensions")
}

android {
    compileSdkVersion(Params.compileSdkVersion)
    buildToolsVersion(Params.buildToolsVersion)

    defaultConfig {
        minSdkVersion(Params.minSdkVersion)
        targetSdkVersion(Params.targetSdkVersion)
    }
}

dependencies {
    implementation(App.Network.retrofit)
    implementation(App.Network.gson)
    implementation(App.Network.rxjava)
    implementation(App.Network.rxandroid)
    implementation(App.Network.rxRetrofitAdapter)
    implementation(App.Network.gsonConverter)

    testImplementation(project(":testutils"))
    testImplementation(UnitTests.junit)
    testImplementation(UnitTests.mockWebServer)
    testImplementation(UnitTests.AssertJ.android)
}
